import { Component, OnInit } from '@angular/core';
import * as signalR from '@microsoft/signalr';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss'],
})
export class LiveComponent implements OnInit {
  connection: signalR.HubConnection;

  constructor() {}

  ngOnInit(): void {
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl('http://192.168.1.102:5000/client')
      // .withAutomaticReconnect()
      .build();

    this.connection.start().then(() => {
      console.log('Started');
      this.connection.stream('Counter', '210').subscribe({
        next: (item) => {
          console.log(item);
        },
        complete: () => {
          console.log('completed');
        },
        error: (err) => {
          // alert('Closed');
        },
      });
    });
  }
}
